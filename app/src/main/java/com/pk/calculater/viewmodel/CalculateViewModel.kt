package com.pk.calculater.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import net.objecthunter.exp4j.ExpressionBuilder

class CalculateViewModel :ViewModel() {

     var values:String=""
     val calculateResult = MutableLiveData<String>()
    val list:ArrayList<String> = ArrayList()


    fun performCalCulation() {

        val text = values
        val expression = ExpressionBuilder(text).build()
        list.add(values)

        val result = expression.evaluate()
        val longResult = result.toLong()
        if (result == longResult.toDouble()) {
            calculateResult.value = longResult.toString()
        } else {
            calculateResult.value  = result.toString()
        }





    }






}