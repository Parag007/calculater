package com.pk.calculater.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LogInViewModel : ViewModel() {


    var username: String = ""
    var password: String = ""
    private val logInResult = MutableLiveData<String>()

    fun getLogInResult(): LiveData<String> = logInResult
    fun performValidation() {

        if (username.isBlank()) {
            logInResult.value = "Invalid username"
            return
        }

        if (password.isBlank()) {
            logInResult.value = "Invalid password"
            return
        }

        if(username.equals("abcd@gmail.com")&&password.equals("123456")){

            logInResult.value="Login Successful"

        }

        logInResult.value = "Invalid credentials"

    }

}