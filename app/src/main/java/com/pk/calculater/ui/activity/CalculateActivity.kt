package com.pk.calculater.ui.activity

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.pk.calculater.R
import com.pk.calculater.ui.adaptor.HistoryAdapter
import com.pk.calculater.databinding.ActivityCalculateBinding
import com.pk.calculater.viewmodel.CalculateViewModel
import kotlinx.android.synthetic.main.activity_calculate.*
import kotlinx.android.synthetic.main.activity_calculate.tvExpressions

class CalculateActivity : AppCompatActivity() {


    private var bindingCalculateActivity: ActivityCalculateBinding? =null
    private lateinit var  viewModel: CalculateViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingCalculateActivity= DataBindingUtil.setContentView(this, R.layout.activity_calculate)
        //init()

        viewModel = ViewModelProviders.of(this).get(CalculateViewModel::class.java)
        bindingCalculateActivity?.lifecycleOwner=this
        bindingCalculateActivity?.viewModel = viewModel

        var historyAdapter= HistoryAdapter(viewModel.list,this)
        bindingCalculateActivity?.myAdapter=historyAdapter


        tvExpressions.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(p0: View?, p1: Int, p2: KeyEvent?): Boolean {
                if (p2?.getAction() === KeyEvent.ACTION_DOWN) {
                    when (p1) {
                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {


                            viewModel!!.performCalCulation()
                            historyAdapter.notifyDataSetChanged()


                            return true
                        }
                        else -> {
                        }
                    }
                }

                return true

            }

        })





    }





//    private var arryListHistry=ArrayList<String>()
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_calculate)
////        init()
//
//    }

//    private fun init() {
//
//       var historyAdapter:HistoryAdapter= HistoryAdapter(arryListHistry,this)
//        recyViewHistory.adapter=historyAdapter
//        val manger=LinearLayoutManager(this)
//        recyViewHistory.layoutManager=manger
//        tvExpressions.setOnKeyListener(object : View.OnKeyListener {
//            override fun onKey(p0: View?, p1: Int, p2: KeyEvent?): Boolean {
//                if (p2?.getAction() === KeyEvent.ACTION_DOWN) {
//                    when (p1) {
//                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
//
//                            val text = tvExpressions.text.toString()
//                            val expression = ExpressionBuilder(text).build()
//
//
//
//                            val result = expression.evaluate()
//                            val longResult = result.toLong()
//                            if (result == longResult.toDouble()) {
//                                tvResult.text = longResult.toString()
//                            } else {
//                                tvResult.text = result.toString()
//                            }
//
//                            arryListHistry.add(tvExpressions.text.toString())
//                            historyAdapter.notifyDataSetChanged()
//                            return true
//                        }
//                        else -> {
//                        }
//                    }
//                }
//
//                return true
//
//            }
//
//        })
//
//
//
//
//    }
}