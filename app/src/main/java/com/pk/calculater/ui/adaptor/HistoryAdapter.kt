package com.pk.calculater.ui.adaptor

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pk.calculater.R
import kotlinx.android.synthetic.main.item_history.view.*

class HistoryAdapter(var listItem:ArrayList<String>,val context:Context) : RecyclerView.Adapter<HistoryAdapter.VievHolder>() {

    class VievHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewValue:TextView=itemView.textviewValue
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VievHolder {

        return VievHolder(LayoutInflater.from(context).inflate(R.layout.item_history,parent,false))

    }

    override fun onBindViewHolder(holder: VievHolder, position: Int) {
        holder.textViewValue.text=listItem.get(position)


    }

    override fun getItemCount(): Int {
        return listItem.size
    }
}