package com.pk.calculater.ui.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.pk.calculater.repo.LogInHandler
import com.pk.calculater.R
import com.pk.calculater.databinding.ActivityLoginBinding
import com.pk.calculater.viewmodel.LogInViewModel

class LogInActivity : AppCompatActivity(), LogInHandler {
    private lateinit var viewModel: LogInViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
        this.viewModel = ViewModelProviders.of(this).get(LogInViewModel::class.java)
        binding.viewModel = viewModel
        binding.handler = this
        viewModel.getLogInResult().observe(this, Observer { result ->
            Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
            if(result.equals("Login Successful"))startActivity(Intent(this, CalculateActivity::class.java))

        })
    }

    override fun onLogInClicked() {
        viewModel.performValidation()
    }

}
